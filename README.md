# LPCyber

Ressources pour la LP Cyber - IUT Vannes

UE4 :
* [Durcissement Linux ANSSI (6h)](https://www.ssi.gouv.fr/guide/recommandations-de-securite-relatives-a-un-systeme-gnulinux/)
* [TP Intrusion (6h)](tp1-intrusion.md)
* [TP IDPS (6h)](tp2-idps.md)
* [TP Messagerie (7,5h)](tp3-mail.md)
* [Cours Authentification forte / Cryptographie (3h)](cours1-authcrypto.md)
* [TP HTTPS / Authentification mutuelle (4,5h)](tp4-https.md)
* [TD Enjeux (4,5h)](td1-enjeux.md)
* [TP Sécurité des BD (12h)](tp5-bd.md)
* [IoT (12h)](tp6-iot.md)
* [LDAP (10,5h)](tp7-ldap.md)
* [Passwords](td-passwords.md)
