-- CREATE USER www WITH PASSWORD 'foo';
-- CREATE DATABASE clientsdb OWNER www;
\c clientsdb
CREATE TABLE clients (id SERIAL, name varchar(15), password varchar(15), role varchar(15), email varchar(25), comment varchar(5000), message varchar(5000));
ALTER TABLE clients OWNER TO www;
INSERT INTO clients( name, password, role) VALUES ('admin', 'admin', 'admin');
INSERT INTO clients( name, password, role) VALUES ('helpdesk1', 'helpdesk1', 'helpdesk');
INSERT INTO clients( name, password, role) VALUES ('helpdesk2', 'helpdesk2', 'helpdesk');
INSERT INTO clients( name, password, role) VALUES ('client1', 'client1', 'client');
INSERT INTO clients( name, password, role) VALUES ('client2', 'client2', 'client');
INSERT INTO clients( name, password, role) VALUES ('client3', 'client3', 'client');
