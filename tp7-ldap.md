# TP 7 : Sécurité des annuaires (10,5h)

Ce TP-projet présente les annuaires LDAP.

Ce TP sera réalisé dans la VM MI-LXC disponible [ici](https://flesueur.irisa.fr/mi-lxc/images/milxc-debian-amd64-1.4.1.ova) (identique à la première période). Avant de lancer la VM, il peut être nécessaire de diminuer la RAM allouée. Par défaut, la VM a 3GO : si vous avez 4GO sur votre machine physique, il vaut mieux diminuer à 2GO, voire 1.5GO pour la VM (la VM devrait fonctionner de manière correcte toujours).

Un rapport par binôme sera à déposer sur Moodle à la fin du TP.

Un [bon tuto vu lors du TP précédent](https://connect.ed-diamond.com/Linux-Pratique/lp-115/installation-et-configuration-d-un-annuaire-openldap).

> Si vous êtes sous Windows et que la VM ne fonctionne pas avec VirtualBox, vous pouvez utiliser à la place VMWare Player. Dans ce cas, il faudra cliquer sur "Retry" lors de l'import puis installer le paquet open-vm-tools-desktop dans la VM pour profiter du redimensionnement automatique du bureau (`apt install open-vm-tools-desktop` dans un shell).

Cheat sheet
===========

Voici un petit résumé des commandes dont vous aurez besoin :

| Commande | Description | Utilisation |
| -------- | ----------- | ----------- |
| print    | Génère la cartographie du réseau | ./mi-lxc.py print |
| attach   | Permet d'avoir un shell sur une machine | ./mi-lxc.py attach iutva-infra |
| display  | Lance un affichage sur la machine cible | ./mi-lxc.py display isp-a-home |
| start    | Démarre la plateforme pédagogique    | ./mi-lxc.py start |
| stop     | Éteint la plateforme pédagogique        | ./mi-lxc.py stop |

Rappel: Vous devez être dans le répertoire `/root/mi-lxc/` pour exécuter ces commandes.

> Si la souris reste bloquée dans une fenêtre virtuelle, appuyez sur CTRL+SHIFT pour la libérer.

> Dans la VM et sur les machines MI-LXC, vous pouvez installer des logiciels supplémentaires. Par défaut, vous avez mousepad pour éditer des fichiers de manière graphique. La VM peut être affichée en plein écran. Si cela ne fonctionne pas, il faut parfois changer la taille de fenêtre manuellement, en tirant dans l'angle inférieur droit, pour que VirtualBox détecte que le redimensionnement automatique est disponible. Il y a une case adéquate (taille d'écran automatique) dans le menu écran qui doit être cochée. Si rien ne marche, c'est parfois en redémarrant la VM que cela peut se déclencher. Mais il *faut* la VM en plein écran.


Étude d'un LDAP existant
========================

L'organisation Target utilise une authentification centralisée basée sur un LDAP. Explorez son usage :
* Qui est le serveur ?
* Trouvez 2 clients, nous allons étudier leur configuration. Cherchez dans `/etc` et ses sous-dossiers (find est votre ami) les fichiers linss-ldap.conf, pam_ldap.conf et nsswitch.conf. Analysez ces fichiers de configuation, cherchez leur usage et notez les quelques lignes qui vous paraissent les paramètres essentiels.
* Analysez les échanges avec Wireshark :
  * Lors d'une authentification (commercial et admin sont des comptes LDAP)
  * Lors d'un `ls -l /home` (Vous devez forcer la résolution de l'uid de commercial ou admin qui sont des comptes LDAP. Attention au cache ! Pour gagner du temps, les clients gardent les dernières réponses en cache et ne refont pas les même requêtes. Pour couper le cache côté client : `service nscd stop`)
* En ligne de commande sur le serveur, à travers les commandes de manipulation ldap (et non les commandes shell `adduser`/`passwd`), trouvez comment :
  * Ajouter un utilisateur (avec ldapadd)
  * Modifier l'uidNumber d'un utilisateur (avec ldapmodify)
  * Modifier le mot de passe d'un utilisateur (avec ldapmodify)

> La commande `ldapsearch -cxD cn=admin,dc=target,dc=milxc -w root -b dc=target,dc=milxc` permet par exemple d'afficher les données existantes. Quelques exemples d'utilisation peuvent être trouvés dans le fichier [mi-lxc/groups/target/ldap/provision.sh](https://github.com/flesueur/mi-lxc/blob/master/groups/target/ldap/provision.sh).


Déploiement d'un LDAP
=====================

Reprenez le TP3 (soit vous avez encore l'infra, soit vous la recréez à partir du sujet) pour avoir l'AS IUTVA. Dans cet AS :
* Ajoutez une machine qui vous servira de serveur LDAP. Une fois démarrée, installez et configurez le serveur LDAP
* Configurez une autre machine de l'AS IUTVA pour être client de ce LDAP (pour l'authentification, PAM, et pour les user/groupes affichés par `ls`, NSS)
* Obtenez un joli certificat TLS :
  * Soit en utilisant la CA ACME déjà explorée
  * Soit en recréant une CA ad hoc interne
  * (le ldap étant un service interne, on peut concevoir de le manipuler avec un certificat à validité interne)
  * Quel matériel cryptographique devez-vous déployer sur le client LDAP ?
  * Mettez à jour la configuration pour faire du LDAPS
* Installez phpldapadmin sur votre machine LDAP pour aider à son administration
* Programmez une sauvegarde régulière vers une autre machine (cron). Modifiez ou supprimez votre annuaire puis restaurez votre sauvegarde, vérifiez.


ACL
===

Les ACL permettent de limiter les accès aux différents champs. Une bonne documentation [ici](https://www.vincentliefooghe.net/content/les-acl-dans-openldap).

Restreignez les accès aux mots de passe comme proposé : seul l'utilisateur et admin peuvent changer les mots de passe. Expérimentez ensuite la restriction d'un autre champ, puis l'annulation de cette restriction.


Durcissement LDAP
=================

Choisissez un guide de durcissement LDAP ([exemple1](https://www.openldap.org/doc/admin26/security.html), [exemple2](https://ldapwiki.com/wiki/Best%20Practices%20for%20LDAP%20Security)). Sans mettre toutes les recommandations en place :
* Analysez les recommandations déjà en place
* Celles qui seraient souhaitables
* Comment seraient-elles mises en place (de manière synthétique, pas des commandes exactes)

Une injection LDAP
==================

Installez, sur votre serveur LDAP ou une machine à côté, un serveur Apache avec PHP et son module LDAP (php-ldap).

Faîtes une page PHP d'annuaire qui demande un login et affiche ses coordonnées (pour l'exemple, vous pouvez juste afficher son uid). Vous pouvez vous inspirer des index.php/do_login.php du TP5 en les adaptant au cas du LDAP. Vous trouverez un peu de doc [ici](https://www.php.net/manual/fr/ldap.using.php) et [là](https://www.php.net/manual/fr/ldap.examples-basic.php).

Enfin, faîtes une injection LDAP sur cette page pour lister l'ensemble des utilisateurs et proposez une remédiation. Une explication des injections LDAP peut être trouvée [ici](https://www.synopsys.com/glossary/what-is-ldap-injection.html).

Essayez de construire une injection LDAP plus complexe (avec intégration de structure de commande (des '()') côté client).
