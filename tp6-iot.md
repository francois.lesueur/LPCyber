# TP 6 : IoT (12h)

L'objectif de ces 12h IoT va être de co-rédiger un livre blanc sur l'IoT coloré sécurité. Le but n'est pas de le publier en ligne (même si rien ne vous l'interdit) mais d'apprendre en le construisant collaborativement.

Voici une proposition de liste de chapitres (modifiable) :
* Objets et leur cycle de vie (Hardware)
* Applications et usages (Software)
* OS (Contiki, RIOT, TinyOS, Windows IoT, ...)
* Protocoles radio (courte portée BLE, Zigbee, etc. et longue portée LoraWAN, Sigfox, NB-IoT)
* Protocoles applicatifs (MQTT, COAP, ...)
* Gestion de l'énergie (Batteries, Harvesting, Biopile, duty-cycle, ...)

Pour cela, nous allons procéder en 4 étapes afin de permettre à chacun·e d'avoir une compréhension globale de ces sujets :
* 3h : en binôme, choix d'un sujet parmi les 6 et rédaction d'une première version du chapitre, sourcé, sur le sujet.
* -> Dépôt de ces versions initiales sur Moodle (format PDF). Puis on change tous les binômes et chacun·e tient un carnet de bord individuel précisant les points sur lesquels iel sera intervenu dans les séances suivantes.
* 2*1h30 : vous retravaillez votre article avec votre nouveau binôme puis son article
* -> Dépôt d'une archive chacun·e contenant 2 **PDF** : les 2 articles retouchés
* 2*1h30 : vous explorez et modifiez 2 autres articles avec un binôme de votre choix (le même ou changement)
* -> Dépôt d'une archive chacun·e contenant 2 **PDF** : les 2 articles retouchés
* 2*1h30 : vous explorez et modifiez 2 autres articles avec un binôme de votre choix (le même ou changement)
* -> Dépôt des carnets de bord individuels sur Moodle (format PDF) + une archive chacun·e contenant 2 **PDF** : les 2 articles retouchés (2 dépôts distincts)
* (Bonus si vous parvenez à vous organiser pour que le système d'alternance des chapitres et binômes fonctionne sans retour de certain·es sur un chapitre déjà vu)

Le résultat final sera ainsi un livre blanc intégralement co-écrit qui doit vous donner un bon aperçu du domaine. L'évaluation sera basée sur votre chapitre initial (après 3h) et votre carnet de bord individuel.
