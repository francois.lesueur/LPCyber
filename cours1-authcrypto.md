# Cours Authentification forte - Cryptographie (3h)


Mode d'emploi distanciel
========================

**Merci de bien lire ces explications avant de démarrer**

Le cours aura lieu en visio BBB (BigBlueButton), les liens seront transmis par mail avant le cours. Un des (nombreux ;) ) intérêts de BBB est de permettre la présence simultanée dans plusieurs salons de discussion audio/vidéo. Il y aura 2 salons :

* Un salon "LP-général", le général, auquel vous devez tous vous connecter en mode micro (mute, mais micro prêt à être activé) et rester tout le long de la séance, qui servira pour les annonces et discussions générales. Il sera limité à ces discussions générales, vous devez garder le son allumé pour entendre les moments d'annonce (ils ne seront pas annoncés à l'écrit, mais le canal vocal de ce BBB général sera suffisamment calme pour que vous puissiez le garder allumé tout le long sans être déconcentré·es dans votre travail).
* Un salon "LP-questions", audio-vidéo également, dans lequel l'enseignant sera en permanence en écoute, et qui est donc l'endroit où aller pour lui poser des questions (et, ainsi, ne pas polluer le général). Vous pouvez rejoindre ce second salon sans quitter le général.

<!-- Sur le BBB général, gardez bien le panneau de gauche ouvert et si possible visible, avec la liste des utilisateurs, et consultez-le régulièrement : c'est ici que vous recevrez les messages privés (des enseignants ou des autres étudiants, relatifs aux messages échangés comme vous le verrez dans la suite du sujet).

Lors des communications à faire avec l'enseignant ou avec d'autres étudiants (décrites dans la suite du sujet), passez bien par ces messages privés BBB afin de ne pas polluer le canal de discussion général : clic sur le nom de la personne, "Démarrer une conversation privée". -->

Déroulement du cours en "autonomie encadrée" :

* Vous avez un travail en autonomie décrit dans la suite de ce document. Ce document vous oriente vers des références externes (à lire, évidemment) et tisse les liens entre ces lectures.
* Durant tout ce travail, je suis disponible dans le salon "Questions", autant pour des éclaircissements que des approfondissements.
* Je vous encourage, évidemment, à échanger aussi en petits groupes via le canal de votre choix (un intérêt de BBB est de permettre de laisser le général ouvert en parallèle de votre autre logiciel)
* Les durées sont données à titre indicatif et seront très variables en fonction de vos questions. Mais c'est a priori assez dense...

Ce que nous allons voir :

* Le principe de l'authentification, l'authentification multi-facteurs et l'authentification forte
* La suite se concentrera sur l'authentification forte par certificat numérique
* Le fonctionnement de la crypto, les classes de crypto, les clés, ... comme outil de base
* Les infrastructures à clés publiques pour lier la crypto à des identités
* Le protocole TLS comme empaquetage de cet ensemble


L'authentification forte (45 minutes)
========================

En informatique, on parle de :

* Identification : association d'un identifiant à un utilisateur
* Authentification : contrôle de cette association (c'est ici que l'on parle de vérification de cet identifiant). Il existe plusieurs méthodes classiques pour cela, décrites [ici](https://fr.wikipedia.org/wiki/Authentification)

L'authentification est couramment un point faible de la sécurité des systèmes. De nombreuses attaques tentent d'usurper des comptes, soit via du phishing pour obtenir des authentifiants valides soit via des attaques directes. Intuitivement, ce mécanisme étant le point d'entrée dans le système et un de ses gardiens essentiels, il est évidemment critique, d'autant plus pour l'authentification des comptes à privilèges (administrateurs).

Afin de rendre l'authentification plus robuste, les authentifications multi-facteurs ou fortes permettent de réduire les risques. Une petite lecture [ici](https://fr.wikipedia.org/wiki/Authentification_forte). L'ANSSI fait la distinction entre les 2 (et apporte en tous cas un élément de réflexion et de choix) dans la [Section 2.5 du guide de l'authentification](https://www.ssi.gouv.fr/uploads/2021/10/anssi-guide-authentification_multifacteur_et_mots_de_passe.pdf#section.2.5).

Pour la suite, nous explorerons le certificat numérique comme facteur cryptographique.

Bases de la crypto (1h)
==================

Vous devez lire le cours de [Ghislaine Labouret](https://web.archive.org/web/20170516210655/http://www.hsc.fr/ressources/cours/crypto/crypto.pdf) <!-- http://www.hsc.fr/ressources/cours/crypto/crypto.pdf https://doc.lagout.org/security/Cryptographie%20.%20Algorithmes%20.%20Steganographie/HSC%20-%20Introduction%20a%20la%20cryptographie.pdf --> (jusqu'à la page 27/32). Vous y verrez les notions de cryptographie symétrique (ex AES), asymétrique (ex RSA), hash, chiffrement, signature ainsi que le problème de la distribution des clés. Ce cours est intéressant car bien construit mais assez ancien (2001). Les notions, principes et difficultés n'ont pas changé depuis, les algorithmes et tailles de clés si : cela vous donne une idée de l'évolution à attendre pendant les 10 prochaines années (hors découverte majeure).

Côté découvertes, on commence par exemple à s'intéresser à la [cryptographie post-quantique](https://fr.wikipedia.org/wiki/Cryptographie_post-quantique) (contentez-vous de lire l'introduction de l'article) : cette cryptographie post-quantique s'exécute sur des ordinateurs traditionnels mais a l'avantage d'être résistante aux attaques des ordinateurs traditionnels et quantiques (à ne pas confondre avec la crypto quantique, donc, qui s'exécuterait sur un ordinateur quantique). Dans votre carrière, vous devriez probablement voir la systématisation de cette cryptographie post-quantique.

À vous de chercher quels sont les algorithmes souhaitables aujourd'hui. Pour les tailles de clés, consultez le site [Key Length](http://www.keylength.com/) qui est très pratique.

<!--
La suite du travail est de découvrir le fonctionnement de RSA (sans entrer, pour l'instant, dans les fondements mathématiques), par exemple sur [Wikipedia](https://fr.wikipedia.org/wiki/Chiffrement_RSA). Prêtez une attention particulière à la génération des clés, aux mécanismes de chiffrement et déchiffrement.
-->

Enfin, le programme [Bullrun](https://fr.wikipedia.org/wiki/Bullrun) donne un bon aperçu des forces et faiblesses de la cryptographie moderne : la partie mathématique est plutôt sûre, les attaques se concentrent sur l'usage (standardisation), le déploiement, l'implémentation, etc.


Infrastructures à clés publiques (PKI) (1h)
=======================================

Le rôle d'une PKI est de lier une clé publique à une identité (typiquement, à une chaîne de caractères intelligible comme une URL `www.acme.org` ou une adresse mail `brice@acme.org`). L'obtention de clés publiques est un service orthogonal au service de sécurité rendu par la cryptographie (ie, un même service, le mail chiffré et signé par exemple, peut-être rendu avec une approche type CA avec S/MIME ou une approche toile de confiance avec PGP).

Vous devez lire la [page anglaise de Wikipedia](https://en.wikipedia.org/wiki/Public_key_infrastructure) sur ce sujet, qui présente différentes formes de PKI (autorités de certifications, toile de confiance, SPKI, blockchain). Attention, la page française n'est pas assez détaillée.<!-- très différente et présente une vision réduites à l'approche CA, c'est uniquement la page anglaise qui fait référence pour ce cours. -->

Vous devez détailler chacune des différentes formes, avec une attention particulière pour les [CA](https://en.wikipedia.org/wiki/Certificate_authority) et le [Web of trust](https://en.wikipedia.org/wiki/Web_of_trust). La PKI DANE/TLSA est très bien décrite et positionnée dans cet [article](http://www.bortzmeyer.org/6698.html). Vous devez enfin lire les [Criticisms](https://en.wikipedia.org/wiki/Public_key_infrastructure#Criticism) de la page principale (et les détails de PKI security issues with X.509, Breach of Comodo CA, Breach of Diginotar).

> Pour comprendre DANE/TLSA qui repose sur DNSSEC, vous devrez peut-être vous rafraichir la mémoire sur le fonctionnement et les différents acteurs du système DNS (typiquement, notions de _registry_, _registrar_, gestion d'une zone et mécanisme de résolution récursif). Ces points ont normalement déjà été vus en TC mais vous pouvez par exemple lire [Sebsauvage](http://sebsauvage.net/comprendre/dns/) jusque "Dans ce cas, ils sont à la fois registry et registrar.", [Bortzmeyer](http://www.bortzmeyer.org/files/cours-dns-cnam-PRINT.pdf) sections "Le protocole DNS" et "Gouvernance" et/ou d'autres ressources équivalentes.


TLS (15 minutes)
===

Dans le TP, nous allons manipuler une CA pour faire du HTTPS (HTTP sur TLS). TLS permet l'authentification mutuelle, une bonne explication [ici](https://tls.ulfheim.net/)

![xkcd](https://imgs.xkcd.com/comics/security.png)
