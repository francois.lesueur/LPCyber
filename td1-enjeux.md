# TD Enjeux éthiques


Pour cette séance, nous allons nous intéresser aux enjeux de société liés à la sécurité et à la surveillance des communications, de l'échelle locale à l'échelle globale. Pour cela, vous devez consacrer le temps de préparation (3 heures, 2 créneaux) à lire des (parties de) documents suivants (si possible pas tous les mêmes, afin de diversifier vos apports lors de la séance de TD qui suivra) :

* [WikipediaEN : Crypto wars](https://en.wikipedia.org/wiki/Crypto_Wars)
* [Listen up, FBI: Juniper code shows the problem with backdoors, _Fahmida Y. Rashid, InfoWorld_](https://www.infoworld.com/article/3018029/virtual-private-network/listen-up-fbi-juniper-code-shows-the-problem-with-backdoors.html)
* [The Risks of “Responsible Encryption”, _Riana Pfefferkorn_](https://cyberlaw.stanford.edu/files/publication/files/2018-02-05%20Technical%20Response%20to%20Rosenstein-Wray%20FINAL.pdf)
* [Keys Under Doormats: Mandating insecurity by requiring government access to all data and communications,_Abelson, Harold; Anderson, Ross; Bellovin, Steven M.; Benaloh, Josh; Blaze, Matt; Diffie, Whitfield; Gilmore, John; Green, Matthew; Landau, Susan; Neumann, Peter G.; Rivest, Ronald L.; Schiller, Jeffrey I.; Schneier, Bruce; Specter, Michael; Weitzner, Daniel J._](https://dspace.mit.edu/bitstream/handle/1721.1/97690/MIT-CSAIL-TR-2015-026.pdf?sequence=8)
* [The Moral Character of Cryptographic Work, _Phillip Rogaway_](http://web.cs.ucdavis.edu/~rogaway/papers/moral-fn.pdf)
* [Decrypting the encryption debate](https://www.nap.edu/catalog/25010/decrypting-the-encryption-debate-a-framework-for-decision-makers)
* [Conférence de clôture du SSTIC 2018, Patrick Pailloux, Directeur technique de la DGSE (vidéo, 1h)](https://www.sstic.org/2018/presentation/2018_cloture/)
* [Tous connectés, tous responsables, _Guillaume Poupard, Directeur général de l’ANSSI_](https://www.liberation.fr/debats/2019/01/21/tous-connectes-tous-responsables_1704409/)
* Sur un mécanisme de séquestre proposé en 2018 par Ray Ozzie, lire [la proposition](https://www.wired.com/story/crypto-war-clear-encryption/), ainsi qu'au moins une critique parmi celles de [Robert Graham](https://blog.erratasec.com/2018/04/no-ray-ozzie-hasnt-solved-crypto.html), [Matthew Green](https://twitter.com/matthew_d_green/status/989222188287954945) et [Steven M. Bellovin, Matt Blaze, Dan Boneh, Susan Landau, and Ronald L. Rivest](https://arstechnica.com/information-technology/2018/05/op-ed-ray-ozzies-crypto-proposal-a-dose-of-technical-reality/)
* [TED: Why Privacy Matters, _Glenn Greenwald_](https://www.ted.com/talks/glenn_greenwald_why_privacy_matters)
* [TEDx: How the NSA betrayed the world's trust -- time to act, _Mikko Hypponen_](https://www.ted.com/talks/mikko_hypponen_how_the_nsa_betrayed_the_world_s_trust_time_to_act#t-798838)
* [Un ex-mercenaire de la surveillance fait son «coming out», _Pierre Alonso_](https://www.liberation.fr/international/un-ex-mercenaire-de-la-surveillance-fait-son-coming-out-20220122_GFFV27NPHJDQNDFHTZIYWCAJQI/) (accessible via l'[UBS/Europresse](https://www.univ-ubs.fr/fr/l-universite-en-pratique/bibliotheque/bibliotheque-numerique/kiosque-de-presse.html) : cliquez sur le lien "Europresse" puis recopiez le titre de l'article dans la boîte de recherche Europresse)

<!-- https://citizenlab.ca/2019/09/annotated-bibliography-dual-use-technologies-network-traffic-management-and-device-intrusion-for-targeted-monitoring/ -->

Cette liste n'est bien sûr pas exhaustive, toutes les suggestions d'ajout sont les bienvenues.

Cette partie mènera à une discussion/débat lors du créneau mardi matin.
